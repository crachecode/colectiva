function hideMenu() {
  document.querySelector('body > header').classList.remove('opened')
}

document.addEventListener('DOMContentLoaded', () => {
  document.querySelector('#sandwich').addEventListener('click', () => {
    document.querySelector('body > header').classList.toggle('opened')
  })
  document.querySelectorAll('header nav a').forEach( (item) => {
    item.addEventListener('click', () => {
      hideMenu()
    })
  })
})
