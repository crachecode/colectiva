---
title: Accueil
draft: false
description: Colectiva
---

## Résidence COLECTIVA

### Association Nébuleuse

COLECTIVA est un programme de résidences orienté vers les domaines des arts visuels et sonores qui a débuté en 2022.  
COLECTIVA s’engage dans des réflexions sur la vie en communauté et a pour but de créer des liens entre les différentes structures culturelles du canton de Genève. COLECTIVA veut aussi visibiliser des artistes travaillant en Amérique Latine et les mettre en lien avec la Suisse pour stimuler les échanges culturels entre ces deux espaces.

![Galiffe](/img/galiffe.jpg)

## Appel à projet - Résidence artisitique

<iframe src="/docs/colectivaxlafonte_FR.pdf" width="100%" height="600px"></iframe>
