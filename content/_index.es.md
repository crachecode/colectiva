---
title: Bienvenida
draft: false
description: Colectiva
---

## Residencia COLECTIVA

### Asociación Nébuleuse

COLECTIVA es un programa de residencias orientado hacia los campos de las artes visuales y sonoras que comenzó en 2022.  
COLECTIVA está comprometido con la reflexión sobre la vida en comunidad y pretende crear vínculos entre las diferentes estructuras culturales del cantón de Ginebra. COLECTIVA también quiere hacer más visibles a lxs artistxs que trabajan en América Latina y relacionarlxs con Suiza para estimular los intercambios culturales entre estas dos zonas geográficas.

![Galiffe](/img/galiffe.jpg)

## Convocatoria de Proyectos - Residencia artística

<iframe src="/docs/colectivaxlafonte_ES.pdf" width="100%" height="600px"></iframe>
