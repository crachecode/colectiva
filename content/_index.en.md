---
title: Home
draft: false
description: Colectiva
---

## COLECTIVA Residency

### Association Nébuleuse

COLECTIVA is a residency program oriented towards the fields of visual and sound arts that started in 2022.  
COLECTIVA engages in reflections on community life, aiming to generate a web between the different cultural structures of the canton of Geneva. COLECTIVA also wants to give visibility to artists working in Latin America and to link them with Switzerland in order to stimulate cultural exchanges between these two countries.

![Galiffe](/img/galiffe.jpg)

## Call for projects - Artistic residency

<iframe src="/docs/colectivaxlafonte_EN.pdf" width="100%" height="600px"></iframe>
